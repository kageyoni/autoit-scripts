#include <KSVDAutoItTestFramework\KSVDAutoItTestFramework.au3>

; ---- ������ ������� ����� ----
Local $bDefaultInstall = IniRead ( $sTestConfigPath, "InstallInformation", "defaultInstall", "$bDefaultInstall" )
Local $bUseExistingLicenseKey  = IniRead ( $sTestConfigPath, "InstallInformation", "licenseKeyInstall ", "$bUseExistingLicenseKey" )
Local $sKSVDSetupFolder = IniRead ( $sCommonConfig, "InstallInformation", "KSVDSetupFolder", "$sKSVDSetupFolder" ) ;���� �� �����, ���������� �� ����������� ��� ��������� KSVD
Local $sInstallPath = IniRead ( $sCommonConfig, "InstallInformation", "installPath", "$sInstallPath" ) ;���������� ��������� KSVD


; ---- ������������� ����������� ���������� ----
Local $sWindowsInstallerFolder = $sKSVDSetupFolder & "\install\windows"
Local $sWindowsInstallerEXEName = "KsvdInstall_" & $sInstallationVersion & ".exe"
Local $sWindowsInstallerEXEPath = $sWindowsInstallerFolder & "\" & $sWindowsInstallerEXEName
Local $sCamera = "Camera1"

;---------------------------------------------------------------------------------
;----- ������ ���������� ����� ---------------------------------------------------
;---------------------------------------------------------------------------------

Local $aSetupParameters[10]
$aSetupParameters[0] = $bDefaultInstall
$aSetupParameters[1] = $bUseExistingLicenseKey
$aSetupParameters[2] = $sInstallationVersion
$aSetupParameters[3] = $sKSVDSetupFolder
$aSetupParameters[4] = $sInstallPath

ActionWindowsInstallKSVD($aSetupParameters)

;---------------------------------------------------------------------------------
;----- ������� �������� ----------------------------------------------------------
;---------------------------------------------------------------------------------

ActionAddCamera($sCamera)
FncCloseArmAdmin()

;---------------------------------------------------------------------------------
;----- ���������� � ������ ���������� KSVD ---------------------------------------
;---------------------------------------------------------------------------------

_FileWriteLog($sLogFile, "������ ������ ��� �������� �������, �������������� ����")
ActionRunArmExportFromArchive("admin", "admin", True)

ActionCloseArmExportFromArchive()




;---------------------------------------------------------------------------------
;----- ���������� ����� ----------------------------------------------------------
;---------------------------------------------------------------------------------

FncTestEnding(True, $TestComplete) ;�������� ���������� �����