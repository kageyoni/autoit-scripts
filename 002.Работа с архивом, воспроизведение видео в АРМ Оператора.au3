#include <KSVDAutoItTestFramework\KSVDAutoItTestFramework.au3>

; ---- ������������� ����������� ���������� ----
Local $sTime =_NowTime () ; ������������� ���������� ������� ��� ����� �� �����
Local $sLogName = "" ; ������������� ���������� ����� ��� ����� ��� ��������
Local $sKeyWord = "" ; ������������� ���������� ��������� ����� ��� ��������
Local $sCamera = "Camera1"
Local $sArchive = "Archive1Auto"
Local $sArchiveGuiName =  IniRead ( $sCamerasConfig, $sCamera, "addedToArchive","$sArchiveGuiName")
Local $sArchiveRecordingStartTime = IniRead ( $sCamerasConfig, $sCamera, "archiveRecordingStartTime","$sArchiveRecordingStartTime")
Local $sExportFromArchiveDir = IniRead ( $sCommonConfig, "BasicInformation", "exportFromArchiveDir","$sExportFromArchiveDir")
Local $sExportOperationNumber = IniRead ( $sCommonConfig, "BasicInformation", "exportOperationNumber","0")
Local $sCameraGuiName = IniRead ( $sCamerasConfig, $sCamera, "cameraGuiName", "$sCameraGuiName")
Local $sCameraName = IniRead ( $sCamerasConfig, $sCamera, "name", "$sCameraName")
Local $sRegionName = $sCameraName
Local $sRegionObjectName = $sCameraName

;---------------------------------------------------------------------------------
;----- ������ ���������� ����� ---------------------------------------------------
;---------------------------------------------------------------------------------
;---- �������� ������, ���������, ������� ----------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmAdmin()
ActionLogInArmAdmin("admin")

ActionAddCamera($sCamera)
ActionAddArchiveGUI($sArchive) ;���������� ���������
ActionAddCameraToArchive($sArchive, $sCamera) ;���������� ��������� � ������

Local $aParameters[5]
$aParameters[0] = $sRegionName ;��� �������
$aParameters[1] = $sRegionObjectName ;��� �������
$aParameters[2] = 1 ;����� ��������
$aParameters[3] = 2 ;������ �����. ������ ��������
$aParameters[4] = 2 ;������ �����. ������ ��������

If $sVersion = "2.8" Then
   ActionAddRegionByScadaScripts($sRegionName)
   ActionAddObjectToRegionByScadaScripts($sRegionName, $sRegionObjectName)
   ActionAddViewPanelByScadaScripts($aParameters)
ElseIf $sVersion = "2.10" Then
   ActionAddRegionByScada($sRegionName)
   ActionAddObjectToRegionByScada($sRegionName, $sRegionObjectName)
   ActionAddCameraToObjectByScada($sRegionName, $sRegionObjectName, $sCamera)
EndIf

FncCloseArmAdmin()

;---------------------------------------------------------------------------------
;---- ���������� � �������� ����� �� ������ --------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmExportFromArchive("admin", "admin", False)

Sleep(600000) ;10 ����� ��������

Local $sCurrentDateTime = _NowCalc() ;��������� �������� �������
Local $aStartTime = FncDateTimeChange("Minus", 7, $sCurrentDateTime) ; ���������� �������� ������ ��� �������� 1-3 - ����, 5-7 - �����
Local $aEndTime = FncDateTimeChange("Minus", 4, $sCurrentDateTime) ; ���������� �������� ������ ��� �������� 1-3 - ����, 5-7 - �����

$sAction = "���� �� ���� �������"
If $sVersion = "2.8" Then
   FncMouseClick("left",550,65,1)
ElseIf $sVersion = "2.10" Then
   FncMouseClick("left",500,65,1)
EndIf

$sAction = "���� ����� ������"
FncSendData($sCameraGuiName)

$sAction = "���� �� �������� ������"
If $sVersion = "2.8" Then
   FncMouseClick("left",68,132,1)
ElseIf $sVersion = "2.10" Then
   FncMouseClick("left",70,135,1)
EndIf

$sAction = "���� �� ���� ������ ���������"
FncMouseClick("left",140,935,1)

$sAction = "������� ������� � ������ ������ �������� ������ HOME"
FncSendData("{HOME}")

_FileWriteLog($sDebugLogFile, "��������� ���� ��� ������������ �����")
$sAction = "������� ������ UP"
FncSendData("{UP}")
$sAction = "������� ������ DOWN"
FncSendData("{DOWN}")

FncSendData($aStartTime[3]) ;����
FncSendData($aStartTime[2]) ;�����
FncSendData($aStartTime[1]) ;���
FncSendData($aStartTime[5]) ;���
FncSendData($aStartTime[6]) ;������
FncSendData($aStartTime[7]) ;�������

$sAction = "������� �� ���� ����� ��������� ������� TAB"
FncSendData("{TAB}")

FncSendData($aEndTime[3]) ;����
FncSendData($aEndTime[2]) ;�����
FncSendData($aEndTime[1]) ;���
FncSendData($aEndTime[5]) ;���
FncSendData($aEndTime[6]) ;������
FncSendData($aEndTime[7]) ;�������

$sAction = "���� �� ���� ������� �������"
FncMouseClick("right",140,985,1)

$sAction = "������� ������� UP"
FncSendData("{UP}")
$sAction = "������� ������� ENTER"
FncSendData("{ENTER}")
$sAction = "�������� ����������� ���� ������� DELETE"
FncSendData("{DEL}")

$sAction = "���� ���������� �������� �� ������"
FncSendData($sExportFromArchiveDir)

;---------------------------------------------------------------------------------
;---- ������ �������� ����� �� ������ --------------------------------------------
;---------------------------------------------------------------------------------

$sAction = "���� �� ���� ������ �������� �������"
FncMouseClick("left",127,64,1)

Sleep(60000) ; �������� 1 ���

$sAction = "����������� �������� ����������� ���������� �������� ����� �� ������"
_FileWriteLog($sLogFile, $sAction)
FncScreenCapture($sKeyScreenPath, $sAction)

;---------------------------------------------------------------------------------
;---- �������� ������������� ������������ ����� �� ����� -------------------------
;---------------------------------------------------------------------------------

Local $sExportFileName
Local $sExportFullFilePath
InnerBuildFileNameAndPath($sExportOperationNumber, $sExportFromArchiveDir, $sCameraGuiName)
InnerCheckFileExists(60, $sExportFullFilePath )

ActionCloseArmExportFromArchive()

;---------------------------------------------------------------------------------
;---- ��������������� ����� � VLC 3.0.3 ------------------------------------------
;---------------------------------------------------------------------------------

Local $sVlcWindowTitle = $sExportFileName & " - ������������������ VLC"

ActionPlayFileInVlc($sExportFileName, $sExportFullFilePath)

;000105_ksvd4_viddown.log
;Video no storage (camera: 106)-������ ���������

;---------------------------------------------------------------------------------
;---- �������� ��������������� ������ � ��������� ������� � ��� ��������� ---------------------
;---------------------------------------------------------------------------------

ActionRunArmOperator("admin")

ActionPlayLiveAndArchiveVideoArmOperator($sCamera)

ActionCloseArmOperator()

;---------------------------------------------------------------------------------
;----- ���������� ����� ----------------------------------------------------------
;---------------------------------------------------------------------------------

FncTestEnding(True, $TestComplete)


Func InnerCheckFileExists($iTimeout, $sExportFullFilePath )
   Local $iTime = 1
   Local $bFileExists = False

   While $iTime <> $iTimeout
	  If FileExists($sExportFullFilePath) Then
		 $bFileExists = True
		 _FileWriteLog($sLogFile, "���� ������� �������� �� ���������")
		 ExitLoop
	  Else
		 $iTime = $iTime + 1
		 Sleep(1000)
	  EndIf
   WEnd

   If $bFileExists = False Then
	  _FileWriteLog($sLogFile, "��������� ������ ��� �������� ����� �� ���������")
	  _FileWriteLog($sLogFile, "�� ������ ���� � ���������� ��������: " & $sExportFullFilePath)
	  _FileWriteLog($sLogFile, "�����, � ������� �������� �������������� �������� ������� �����: " & $iTime & " ���.")
	  FncTestEnding(False, "������ ��� �������� ����� �� ���������")
   EndIf
EndFunc

Func InnerBuildFileNameAndPath($sExportOperationNumber, $sExportFromArchiveDir, $sCameraGuiName)
   $sExportOperationNumber = $sExportOperationNumber + 1
   IniWrite ( $sCommonConfig, "BasicInformation", "exportOperationNumber",$sExportOperationNumber) ;������ � ������
   If $sExportOperationNumber < 10 Then
	  $sExportOperationNumber = "00000" & $sExportOperationNumber
   ElseIf $sExportOperationNumber < 100 Then
	  sExportOperationNumber = "0000" & $sExportOperationNumber
   ElseIf $sExportOperationNumber < 1000 Then
	  sExportOperationNumber = "000" & $sExportOperationNumber
   EndIf
   $sExportFileName = $sCameraGuiName & "_" & $sExportOperationNumber & ".mp4"
   $sExportFullFilePath = $sExportFromArchiveDir & "\" & $sExportFileName
   _FileWriteLog($sLogFile, "��������� ��� ������������ ����� �� ���������: " & $sExportFileName)
EndFunc

Func InnerBuildVLCTitle($sCameraGuiName)

   Local $sExportOperationNumber = IniRead ( $sCommonConfig, "BasicInformation", "exportOperationNumber","0")
   If $sExportOperationNumber < 10 Then
	  $sExportOperationNumber = "00000" & $sExportOperationNumber
   ElseIf $sExportOperationNumber < 100 Then
	  sExportOperationNumber = "0000" & $sExportOperationNumber
   ElseIf $sExportOperationNumber < 1000 Then
	  sExportOperationNumber = "000" & $sExportOperationNumber
   EndIf

   Local $sVLCTitle = $sCameraGuiName & "_" & $sExportOperationNumber & ".mp4 - ������������������ VLC"
   Return $sVLCTitle
EndFunc