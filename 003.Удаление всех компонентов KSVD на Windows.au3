#include <KSVDAutoItTestFramework\KSVDAutoItTestFramework.au3>

;---------------------------------------------------------------------------------
;----- ������ ���������� ����� ---------------------------------------------------
;---------------------------------------------------------------------------------

ActionRemoveKSVD()

;---------------------------------------------------------------------------------
;----- �������� �������� ����������, ��� ��� ���������� KSVD ---------------------
;---------------------------------------------------------------------------------

if FileExists($sInstallPath)Then
    _FileWriteLog($sLogFile, "���������� " & $sInstallPath & " �� ������� ����� ���������� ������ ��������������")
    Local $aFileList = _FileListToArray($sInstallPath, Default, Default, False)

    If @error = 4 Then
        FncTestEnding(False, "���������� " & $sInstallPath & " ������")
    EndIf

    if $aFileList[0] = 1 then

        if $aFileList[1] = "Downloads" Then
            _FileWriteLog($sLogFile, "� ���������� " & $sInstallPath & " �������� ������ ����� Downloads")
            Local $aDownloadsDirFileList = _FileListToArray($sInstallPath & "\Downloads", Default, Default, False)

            If @error = 4 Then
                FncTestEnding(False, "����� Downloads ������")
            EndIf
            
            If $aDownloadsDirFileList[0] > 0 Then
                FncTestEnding(True, $TestComplete)
            EndIf

        Else
            FncTestEnding(False, "����� ���������� ������ �������������� �������� ���������� ����� ��� �����(�������� ����� Downloads)")
        endif

    ElseIf $aFileList[0] > 1 then
        FncTestEnding(False, "����� ���������� ������ �������������� �������� ���������� ����� ��� �����")
    Else
        FncTestEnding(False, "�������� ������������� �������� ����������. $aFileList[0] = " & $aFileList[0])
    EndIf

Else
    FncTestEnding(True, "���������� " & $sInstallPath & " ������� ������� ����� ���������� ������ ��������������")
EndIf