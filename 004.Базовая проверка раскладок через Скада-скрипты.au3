#include <KSVDAutoItTestFramework\KSVDAutoItTestFramework.au3>

; ---- Инициализация необходимых переменных ----
Local $sRegionName = "AutotestScadaScripts1"
Local $sRegionObjectName = "AutotestScadaScripts1"
Local $sRegionName2 = "AutotestScadaScripts2"
Local $sRegionObjectName2 = "AutotestScadaScripts2"
Local $sCamera1 = "AutotestScadaScripts1"
Local $sCamera2 = "AutotestScadaScripts2"

Local $aParameters[5]
$aParameters[0] = $sRegionName ;Имя Региона
$aParameters[1] = $sRegionObjectName ;Имя Объекта
$aParameters[2] = 1 ;Номер монитора
$aParameters[3] = 5 ;Размер сетки. Первый параметр
$aParameters[4] = 5 ;Размер сетки. Второй параметр

Local $aParameters2[5]
$aParameters2[0] = $sRegionName2 ;Имя Региона
$aParameters2[1] = $sRegionObjectName2 ;Имя Объекта
$aParameters2[2] = 2 ;Номер монитора
$aParameters2[3] = 5 ;Размер сетки. Первый параметр
$aParameters2[4] = 5 ;Размер сетки. Второй параметр

;---------------------------------------------------------------------------------
;----- Начало выполнения теста ---------------------------------------------------
;---------------------------------------------------------------------------------
;----- Добавление Камер, Раскладки 1 для монитора 1 ------------------------------
;---------------------------------------------------------------------------------

ActionRunArmAdmin()
ActionLogInArmAdmin("admin")
ActionAddCamera($sCamera1)

ActionAddRegionByScadaScripts($sRegionName)
ActionAddObjectToRegionByScadaScripts($sRegionName, $sRegionObjectName)
ActionAddViewPanelByScadaScripts($aParameters)
FncCloseArmAdmin()

;---------------------------------------------------------------------------------
;----- Проверка Раскладки1 в АРМ Оператора ---------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmOperator("admin")
ActionOpenViewPanelInArmOperator($sRegionName, 1)
ActionCloseArmOperator()

;---------------------------------------------------------------------------------
;----- Добавление Раскладки 2 для монитора 2 -------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmAdmin()
ActionLogInArmAdmin("admin")
ActionAddCamera($sCamera2)

;удалить предыдущую раскладку
ActionRemoveRegion($sRegionName, "ScadaScript")

ActionAddRegionByScadaScripts($sRegionName2)
ActionAddObjectToRegionByScadaScripts($sRegionName2, $sRegionObjectName2)
ActionAddViewPanelByScadaScripts($aParameters2)
FncCloseArmAdmin()

;---------------------------------------------------------------------------------
;----- Проверка Раскладки1 в АРМ Оператора ---------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmOperator("admin")
ActionOpenViewPanelInArmOperator($sRegionName2, 2)
ActionCloseArmOperator()

;---------------------------------------------------------------------------------
;----- Завершение теста ----------------------------------------------------------
;---------------------------------------------------------------------------------

FncTestEnding(True, $TestComplete)