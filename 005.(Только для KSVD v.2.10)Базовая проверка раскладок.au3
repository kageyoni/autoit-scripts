#include <KSVDAutoItTestFramework\KSVDAutoItTestFramework.au3>

; ---- Инициализация необходимых переменных ----
Local $sRegionName = "AutotestViewPanelScada"
Local $sRegionObjectName = "AutotestObjectViewPanelScada"
Local $sViewPanelName = "AutotestViewPanel"
Local $sRegionName2 = "AutotestViewPanelScada2"
Local $sRegionObjectName2 = "AutotestObjectViewPanelScada2"
Local $sViewPanelName2 = "AutotestViewPanel2"
Local $sCameraForViewPanel_1 = "Camera1"
Local $sCameraForViewPanel_2 = "Camera2"
Local $sCameraForViewPanel_3 = "Camera3"

Local $aParameters[6]
$aParameters[0] = $sRegionName ;Имя Региона
$aParameters[1] = $sRegionObjectName ;Имя Объекта
$aParameters[2] = $sViewPanelName ;Имя Раскладки
$aParameters[3] = 2 ;Номер монитора
$aParameters[4] = 5 ;Размер сетки. Первый параметр
$aParameters[5] = 5 ;Размер сетки. Второй параметр

Local $aParameters2[6]
$aParameters2[0] = $sRegionName2 ;Имя Региона
$aParameters2[1] = $sRegionObjectName2 ;Имя Объекта
$aParameters2[2] = $sViewPanelName2 ;Имя Раскладки
$aParameters2[3] = 1 ;Номер монитора
$aParameters2[4] = 2 ;Размер сетки. Первый параметр
$aParameters2[5] = 2 ;Размер сетки. Второй параметр


;---------------------------------------------------------------------------------
;----- Начало выполнения теста ---------------------------------------------------
;---------------------------------------------------------------------------------
;----- Добавление Камер, Регионов, раскладок -------------------------------------
;---------------------------------------------------------------------------------

If $sVersion = "2.8" Then
    FncTestEnding(False, "Функционал добавления регионов и раскладок через вкладку СКАДА не реализован в KSVD v2.8")
EndIf

ActionRunArmAdmin()
ActionLogInArmAdmin("admin")

ActionAddCamera($sCameraForViewPanel_1)
ActionAddCamera($sCameraForViewPanel_2)
ActionAddCamera($sCameraForViewPanel_3)

ActionAddRegionByScada($sRegionName) ;Добавление региона
ActionAddObjectToRegionByScada($sRegionName, $sRegionObjectName) ;Добавление Объекта Региона
ActionAddCameraToObjectByScada($sRegionName, $sRegionObjectName, $sCameraForViewPanel_1) ;Добавление первой Камеры к Объекту Региона
ActionAddCameraToObjectByScada($sRegionName, $sRegionObjectName, $sCameraForViewPanel_2) ;Добавление второй Камеры к Объекту Региона

ActionAddRegionByScada($sRegionName2) ;Добавление региона
ActionAddObjectToRegionByScada($sRegionName2, $sRegionObjectName2) ;Добавление Объекта Региона
ActionAddCameraToObjectByScada($sRegionName2, $sRegionObjectName2, $sCameraForViewPanel_3) ;Добавление Камеры к Объекту Региона

ActionAddViewPanelByScada($aParameters)

FncSavePrintScreen("Контрольный скрин раскладки1 в АРМ Администратора")

ActionAddViewPanelByScada($aParameters2)

FncSavePrintScreen("Контрольный скрин раскладки2 в АРМ Администратора")

FncCloseArmAdmin()

;---------------------------------------------------------------------------------
;----- Проверка Раскладок в АРМ Оператора ----------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmOperator("admin")
ActionOpenViewPanelInArmOperator($sViewPanelName2, 1)
ActionCloseArmOperator()


ActionRunArmOperator("admin")
ActionOpenViewPanelInArmOperator($sViewPanelName, 2)
ActionCloseArmOperator()

;---------------------------------------------------------------------------------
;----- Завершение теста ----------------------------------------------------------
;---------------------------------------------------------------------------------

FncTestEnding(True, $TestComplete)