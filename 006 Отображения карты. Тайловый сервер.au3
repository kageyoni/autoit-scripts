#include <KSVDAutoItTestFramework\KSVDAutoItTestFramework.au3>

; ---- Инициализация необходимых переменных ----
Local $sRegionName = "TileServerAutoTest"

;---------------------------------------------------------------------------------
;----- Начало выполнения теста ---------------------------------------------------
;---------------------------------------------------------------------------------
;----- Добавление Региона и Тайлового сервера ------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmAdmin()
ActionLogInArmAdmin("admin")

If $sVersion = "2.8" Then
    ActionAddRegionByScadaScripts($sRegionName)
ElseIf $sVersion = "2.10" Then
    ActionAddRegionByScada($sRegionName)
EndIf

ActionAddTileServer("autotest1TileServer")

If $sVersion = "2.8" Then
    ActionAddRegionByScadaScripts($sRegionName)
ElseIf $sVersion = "2.10" Then ;----- Для 2.10 дополнительная проверка в АРМ Администратора
    ActionAddRegionByScada($sRegionName)
    FncCloseArmAdmin();---- Перезапуск АРМ Администратора (без перезапуска не отобразится карта региона) ----
    ActionRunArmAdmin()
    ActionLogInArmAdmin("admin")
    ActionFindRegionInScada($sRegionName)
    Sleep(2000)
    $sAction = "Контрольный скриншот в АРМ Администратора"
    _FileWriteLog($sLogFile, $sAction)
    FncScreenCapture($sKeyScreenPath, $sAction)
EndIf

FncCloseArmAdmin()

;---------------------------------------------------------------------------------
;----- Проверка отображения карты в АРМ Оператора --------------------------------
;---------------------------------------------------------------------------------

ActionRunArmOperator ("admin")

If $sVersion = "2.8" Then
    ;nothing to do
    ;Карта должна отобразиться сразу же после авторизации в АРМ Оператора
ElseIf $sVersion = "2.10" Then
    FncMouseClick("left",800,600,1)
    $sAction = "Клик по вкладке Карта Объектов"
    FncMouseClick("left",25,125,1)
EndIf

Sleep(2000)
$sAction = "Контрольный скриншот в АРМ Оператора"
_FileWriteLog($sLogFile, $sAction)
FncScreenCapture($sKeyScreenPath, $sAction)

ActionCloseArmOperator()

;---------------------------------------------------------------------------------
;----- Завершение теста ----------------------------------------------------------
;---------------------------------------------------------------------------------

FncTestEnding(True, $TestComplete)