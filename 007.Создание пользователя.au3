#include <KSVDAutoItTestFramework\KSVDAutoItTestFramework.au3>

; ---- Инициализация необходимых переменных ----

Local $sUser = "user"
Local $sAdmin1 = "administrator1"
Local $sNotAddedUser = "NotAddedUser"

;---------------------------------------------------------------------------------
;----- Начало выполнения теста ---------------------------------------------------
;---------------------------------------------------------------------------------
;----- Добавление Пользователей --------------------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmAdmin()
ActionLogInArmAdmin("admin")

ActionAddUser($sUser)
ActionAddUser($sAdmin1)

FncCloseArmAdmin()

;---------------------------------------------------------------------------------
;----- Проверка логина в АРМ Администратора --------------------------------------
;---------------------------------------------------------------------------------

$bExitIfAuthFail = False ; Если авторизация не успешна, значение false позволит дальше выполнять тест
_FileWriteLog($sLogFile, "$bExitIfAuthFail = " & $bExitIfAuthFail)

_FileWriteLog($sLogFile, "Авторизация должна быть неуспешна, т.к. пользователь "& $sUser &" не имеет прав администратора")
ActionRunArmAdmin()
ActionLogInArmAdmin($sUser)
FncScreenCapture($sKeyScreenPath, "Авторизация пользователя " & $sUser & " в АРМ Администратора")
FncCloseArmAdmin()

$bExitIfAuthFail = True
_FileWriteLog($sLogFile, "Авторизация пользователя "& $sAdmin1 &" должна быть успешна")
ActionRunArmAdmin()
ActionLogInArmAdmin($sAdmin1)
FncScreenCapture($sKeyScreenPath, "Авторизация пользователя " & $sAdmin1 & " в АРМ Администратора")
FncCloseArmAdmin()

$bExitIfAuthFail = False
_FileWriteLog($sLogFile, "Авторизация пользователя "& $sNotAddedUser &" должна быть неуспешна, т.к. пользователь не добавлен в GUI")
ActionRunArmAdmin()
ActionLogInArmAdmin($sNotAddedUser)
FncScreenCapture($sKeyScreenPath, "Авторизация пользователя " & $sNotAddedUser & " в АРМ Администратора")
FncCloseArmAdmin()

;---------------------------------------------------------------------------------
;----- Проверка логина в АРМ Оператора -------------------------------------------
;---------------------------------------------------------------------------------

$bExitIfAuthFail = True
_FileWriteLog($sLogFile, "Авторизация пользователя "& $sUser &" должна быть успешна")
ActionRunArmOperator ($sUser) 
FncScreenCapture($sKeyScreenPath, "Авторизация пользователя " & $sUser & " в АРМ Оператора")
ActionCloseArmOperator()

$bExitIfAuthFail = False
_FileWriteLog($sLogFile, "Авторизация пользователя "& $sNotAddedUser &" должна быть НЕ успешна")
ActionRunArmOperator ($sNotAddedUser)
FncScreenCapture($sKeyScreenPath, "Авторизация пользователя " & $sNotAddedUser & " в АРМ Оператора")
ActionCloseArmOperator()

$bExitIfAuthFail = True
_FileWriteLog($sLogFile, "Авторизация пользователя "& $sAdmin1 &" должна быть успешна")
ActionRunArmOperator ($sAdmin1)
FncScreenCapture($sKeyScreenPath, "Авторизация пользователя " & $sAdmin1 & " в АРМ Оператора")
ActionCloseArmOperator()

;---------------------------------------------------------------------------------
;----- Завершение теста ----------------------------------------------------------
;---------------------------------------------------------------------------------

FncTestEnding(True, $TestComplete)