#include <KSVDAutoItTestFramework\KSVDAutoItTestFramework.au3>

; ---- Инициализация необходимых переменных ----
Local $sCameraForViewPanel_3 = "Camera3"
Local $sRegionName = "AutotestArmSettingsRegion"
Local $sRegionObjectName = "AutotestArmSettingsRegionObject"
Local $sViewPanelName = "AutotestArmSettingsViewPanel"

;---------------------------------------------------------------------------------
;----- Начало выполнения теста ---------------------------------------------------
;---------------------------------------------------------------------------------
;---- Создание Камеры, Региона, Раскладки ----------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmAdmin()
ActionLogInArmAdmin("admin")
ActionAddCamera($sCameraForViewPanel_3)

If $sVersion = "2.8" Then
    Local $aParameters[6]
    $aParameters[0] = "Camera3Auto" ;Имя Региона
    $aParameters[1] = "Camera3Auto" ;Имя Объекта
    $aParameters[2] = 1 ;Номер монитора
    $aParameters[3] = 2 ;Размер сетки. Первый параметр
    $aParameters[4] = 2 ;Размер сетки. Второй параметр
    ActionAddRegionByScadaScripts("Camera3Auto")
    ActionAddObjectToRegionByScadaScripts("Camera3Auto", "Camera3Auto")
    ActionAddViewPanelByScadaScripts($aParameters)
ElseIf $sVersion = "2.10" Then
    Local $aParameters[6]
    $aParameters[0] = $sRegionName ;Имя Региона
    $aParameters[1] = $sRegionObjectName ;Имя Объекта
    $aParameters[2] = $sViewPanelName ;Имя Раскладки
    $aParameters[3] = 1 ;Номер монитора
    $aParameters[4] = 2 ;Размер сетки. Первый параметр
    $aParameters[5] = 2 ;Размер сетки. Второй параметр
    ActionAddRegionByScada($sRegionName) ;Добавление региона
    ActionAddObjectToRegionByScada($sRegionName, $sRegionObjectName) ;Добавление Объекта Региона
    ActionAddCameraToObjectByScada($sRegionName, $sRegionObjectName, $sCameraForViewPanel_3) ;Добавление Камеры к Объекту Региона
    ActionAddViewPanelByScada($aParameters)
EndIf


FncCloseArmAdmin()

;---------------------------------------------------------------------------------
;----- Работа с АРМ Настройки KSVD -----------------------------------------------
;---------------------------------------------------------------------------------
;---- Изменение стандартных настроек ---------------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmSettings()

If $sVersion = "2.8" Then
    Local $aParameters[3] = ["LocalUser", "2.7", "Fill"]
ElseIf $sVersion = "2.10" Then
    Local $aParameters[7] = ["LocalUser", "2.7", "Fill", True, "UP", False, False]
EndIf

ActionChangeArmSettings($aParameters)

;---------------------------------------------------------------------------------
;----- Проверка выбранных настроек в АРМ Оператора -------------------------------
;---------------------------------------------------------------------------------

ActionRunArmOperator ("admin")

If $sVersion = "2.8" Then
    ActionFindPlayCameraShowPanelArmOperator($sCameraForViewPanel_3)
ElseIf $sVersion = "2.10" Then
    ActionFindViewPanelInArmOperator($sViewPanelName)

    ActionHideShowArmOperatorWindow("hide")
    FncCheckAndActivateWindow("Program Manager")
    $sAction = "Клик по окну видео для отображение панели управления"
    FncMouseClick("left",180,80,1)

    Sleep(1000)

    $sAction = "Настройки 1 в АРМ Оператора"
    _FileWriteLog($sLogFile, $sAction)
    FncScreenCapture($sKeyScreenPath, $sAction)

    ActionHideShowArmOperatorWindow("show")
EndIf

ActionCloseArmOperator()



;---------------------------------------------------------------------------------
;----- Работа с АРМ Настройки KSVD -----------------------------------------------
;---------------------------------------------------------------------------------
;----- Возврат стандартных настроек ----------------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmSettings()

If $sVersion = "2.8" Then
    Local $aParameters2[3] = ["LocalUser", "2.8+", "KeepAspectRatio"]
ElseIf $sVersion = "2.10" Then
    Local $aParameters2[7] = ["LocalUser", "Modern", "KeepAspectRatio", False, "DOWN", True, True]
EndIf

ActionChangeArmSettings($aParameters2)

;---------------------------------------------------------------------------------
;----- Проверка выбранных настроек в АРМ Оператора -------------------------------
;---------------------------------------------------------------------------------

ActionRunArmOperator ("admin")

If $sVersion = "2.8" Then
    ActionFindPlayCameraShowPanelArmOperator($sCameraForViewPanel_3)
ElseIf $sVersion = "2.10" Then
    ActionFindViewPanelInArmOperator($sViewPanelName)

    ActionHideShowArmOperatorWindow("hide")
    FncCheckAndActivateWindow("Program Manager")
    $sAction = "Клик по окну видео для отображение панели управления"
    FncMouseClick("left",180,80,1)

    Sleep(1000)

    $sAction = "Настройки 2 в АРМ Оператора"
    _FileWriteLog($sLogFile, $sAction)
    FncScreenCapture($sKeyScreenPath, $sAction)

    ActionHideShowArmOperatorWindow("show")
EndIf

ActionCloseArmOperator()

;---------------------------------------------------------------------------------
;----- Завершение теста ----------------------------------------------------------
;---------------------------------------------------------------------------------

FncTestEnding(True, $TestComplete)