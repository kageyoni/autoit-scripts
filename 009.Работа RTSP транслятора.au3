#include <KSVDAutoItTestFramework\KSVDAutoItTestFramework.au3>

; ---- ������������� ����������� ���������� ----

Local $sCamera = "Camera1"
Local $sRtspRetranslator = "rtspRetran1auto"

;---------------------------------------------------------------------------------
;----- ������ ���������� ����� ---------------------------------------------------
;---------------------------------------------------------------------------------
;---- �������� ������, �������������, ���������� ������ � ������������� ----------
;---------------------------------------------------------------------------------

ActionRunArmAdmin()
ActionLogInArmAdmin("admin")

ActionAddRtspRetranslatorGUI($sRtspRetranslator)

ActionAddCamera($sCamera)

ActionAddRtspRetranslatorToCamera($sRtspRetranslator, $sCamera)

;---------------------------------------------------------------------------------
;---- ��������������� ������ � VLC -----------------------------------------------
;---------------------------------------------------------------------------------

Local $sLocalIp = FncGetLocalIP()
Local $sRetranslatedCameraID = IniRead($sCamerasConfig, $sCamera, "id",  "$iID")
Local $sRetranslatorPort = IniRead($sRTSPConfig, $sRtspRetranslator, "port", "$sPort")
Local $sURI = "rtsp://" & $sLocalIp & ":" & $sRetranslatorPort & "/" & $sRetranslatedCameraID

If ActionPlayStreamInVlc($sURI) = False Then
    FncTestEnding(False,"�� ������� ������������� �����-�����")
EndIf


;---------------------------------------------------------------------------------
;----- ���������� ����� ----------------------------------------------------------
;---------------------------------------------------------------------------------

FncTestEnding(True, $TestComplete)