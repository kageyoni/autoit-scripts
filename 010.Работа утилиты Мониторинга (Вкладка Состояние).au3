#include <KSVDAutoItTestFramework\KSVDAutoItTestFramework.au3>

; ---- Инициализация необходимых переменных ----
Local $sCamera = "Camera1"
Local $sCameraName = IniRead ( $sCamerasConfig, $sCamera, "name", "$sCameraName")
Local $sArchive = "Archive1Auto"

;---------------------------------------------------------------------------------
;----- Начало выполнения теста ---------------------------------------------------
;---------------------------------------------------------------------------------
;---- Создание Камеры и Хранилища ------------------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmAdmin()
ActionLogInArmAdmin("admin")

;---- Добавление Камер и Хранилища, чтобы было что мониторить ----
ActionAddCamera($sCamera )
ActionAddArchiveGUI($sArchive) ;Добавление Хранилища
ActionAddCameraToArchive($sArchive, $sCamera ) ;Добавление Хранилища к Камере
sleep(120000)
FncCloseArmAdmin()

;---------------------------------------------------------------------------------
;---- Работа с АРМ Мониторинга ---------------------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmMonitor("admin")

$sAction = "Клик по вкладке Состояние" 
FncMouseClick("left",40,60,1)

$sAction = "Клик по кнопке Развернуть"
If $sVersion = "2.8"  Then
    FncMouseClick("left",390,90,1)
ElseIf $sVersion = "2.10" Then
    FncMouseClick("left",425,100,1)
EndIf

$sAction = "Клик по полю поиска"
If $sVersion = "2.8"  Then
    FncMouseClick("left",100,90,1)
ElseIf $sVersion = "2.10" Then
    FncMouseClick("left",100,100,1)
EndIf

FncSendData($sCameraName)

$sAction = "Клик по Камере " & $sCameraName
If $sVersion = "2.8"  Then
    FncMouseClick("left",105,155,1)
ElseIf $sVersion = "2.10" Then
    FncMouseClick("left",105,165,1)
EndIf

FncScreenCapture($sKeyScreenPath, "Контрольный скрин временной шкалы")

Local $iYCOORD
If $sVersion = "2.8"  Then
    $iYCOORD = 670
ElseIf $sVersion = "2.10" Then
    $iYCOORD = 393
EndIf


Local $sScreenName = "Манипуляции с линией времени "
Local $iInnerScreenNumber = 1
FncScreenCapture($sDebugScreenPath, $sScreenName & $iInnerScreenNumber )
FncMouseClick("left",340,$iYCOORD,1)
$iInnerScreenNumber +=1
FncScreenCapture($sDebugScreenPath, $sScreenName & $iInnerScreenNumber)
MouseMove(340,$iYCOORD)
$iInnerScreenNumber +=1
FncScreenCapture($sDebugScreenPath, $sScreenName & $iInnerScreenNumber)
MouseDown("left")
$iInnerScreenNumber +=1
FncScreenCapture($sDebugScreenPath, $sScreenName & $iInnerScreenNumber)
MouseMove(100,$iYCOORD)
$iInnerScreenNumber +=1
FncScreenCapture($sDebugScreenPath, $sScreenName & $iInnerScreenNumber)
MouseUp("left")
$iInnerScreenNumber +=1
FncScreenCapture($sDebugScreenPath, $sScreenName & $iInnerScreenNumber)
Sleep(1000)
MouseDown("left")
$iInnerScreenNumber +=1
FncScreenCapture($sDebugScreenPath, $sScreenName & $iInnerScreenNumber)
If $sVersion = "2.8"  Then
    MouseMove(550,$iYCOORD)
ElseIf $sVersion = "2.10" Then
    MouseMove(600,$iYCOORD)
EndIf
$iInnerScreenNumber +=1
FncScreenCapture($sDebugScreenPath, $sScreenName & $iInnerScreenNumber)
MouseUp("left")
$iInnerScreenNumber +=1
FncScreenCapture($sDebugScreenPath, $sScreenName & $iInnerScreenNumber)

FncScreenCapture($sKeyScreenPath, "Контрольный скрин АРМ Мониторинга")

If $sVersion = "2.8"  Then
    FncCloseWindow($sArmMonitorWindowName, "Qt5QWindowIcon") 
ElseIf $sVersion = "2.10" Then
    FncCloseWindow($sArmMonitorWindowName) 
EndIf

;---------------------------------------------------------------------------------
;----- Завершение теста ----------------------------------------------------------
;---------------------------------------------------------------------------------

FncTestEnding(True, $TestComplete)