#include <KSVDAutoItTestFramework\KSVDAutoItTestFramework.au3>

; ---- Инициализация необходимых переменных ----
Local $sRegionName = "RegionPlanAutoTest"
Local $sRegionObjectName = "ObjectPlanAutoTest"
Local $sPlanFileName = $sResourcesDir & "plan.jpg"

;---------------------------------------------------------------------------------
;----- Начало выполнения теста ---------------------------------------------------
;---------------------------------------------------------------------------------
;---- Создание Региона, добавление Плана -----------------------------------------
;---------------------------------------------------------------------------------

If $sVersion = "2.8" Then
    FncTestEnding(False, "Функционал отображения Плана Объекта не реализован в KSVD v2.8")
EndIf

ActionRunArmAdmin()
ActionLogInArmAdmin("admin")

ActionAddRegionByScada($sRegionName)
ActionAddObjectToRegionByScada($sRegionName, $sRegionObjectName) ;Добавление Объекта Региона
ActionAddPlanToRegionObject($sRegionName, $sRegionObjectName, $sPlanFileName)

FncCloseArmAdmin()

;---------------------------------------------------------------------------------
;---- Проверка отображения Плана в АРМ Оператора ---------------------------------
;---------------------------------------------------------------------------------

ActionRunArmOperator("admin")

ActionFindRegionObjectInArmOperator($sRegionObjectName)

sleep (2000)

FncScreenCapture($sKeyScreenPath, "Контрольный скрин объекта")

ActionCloseArmOperator()

;---------------------------------------------------------------------------------
;----- Завершение теста ----------------------------------------------------------
;---------------------------------------------------------------------------------

FncTestEnding(True, $TestComplete)