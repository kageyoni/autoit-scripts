#include <KSVDAutoItTestFramework\KSVDAutoItTestFramework.au3>

;---- Чтение конфига теста
Local $sActualVersionKSVD = IniRead($sTestConfigPath, "TestInformation", "actualVersionKSVD", "$sActualVersionKSVD")
Local $sPreviousVersionKSVD = IniRead($sTestConfigPath, "TestInformation", "previousVersionKSVD", "$sPreviousVersionKSVD")
Local $sPreviousVersionKSVDSetupFolder = IniRead($sTestConfigPath, "TestInformation", "previousVersionKSVDSetupFolder", "$sPreviousVersionKSVDSetupFolder")

;---------------------------------------------------------------------------------
;----- Начало выполнения теста ---------------------------------------------------
;---------------------------------------------------------------------------------
;----- Установка предыдущей версии KSVD ------------------------------------------
;---------------------------------------------------------------------------------

Local $bUseExistingLicenseKey = "False"

Local $aSetupParameters[10]
$aSetupParameters[0] = "True" ;$bDefaultInstall
$aSetupParameters[1] = $bUseExistingLicenseKey
$aSetupParameters[2] = $sPreviousVersionKSVD
$aSetupParameters[3] = $sPreviousVersionKSVDSetupFolder
$aSetupParameters[4] = $sInstallPath

ActionWindowsInstallKSVD($aSetupParameters)

;---------------------------------------------------------------------------------
;----- Проверка текущей версии ---------------------------------------------------
;---------------------------------------------------------------------------------

ActionCheckKsvdVersion($sPreviousVersionKSVD, True)

;---------------------------------------------------------------------------------
;----- Добавление Точки Обновления -----------------------------------------------
;---------------------------------------------------------------------------------

ActionAddUpdateSourse("updateFromHTTPToActualVersion", "")
sleep(5000)

;---------------------------------------------------------------------------------
;----- Установка Обновления ------------------------------------------------------
;---------------------------------------------------------------------------------

FncCheckAndActivateWindow("Обновление")

$sAction = "Клик по кнопке Установить без задержек"
FncMouseClick("left",60,110,1)
sleep(60000)

;---------------------------------------------------------------------------------
;----- Проверка текущей версии ---------------------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmAdmin()
ActionLogInArmAdmin("admin")
ActionCheckKsvdVersion($sActualVersionKSVD, True)

;---------------------------------------------------------------------------------
;----- Завершение теста ----------------------------------------------------------
;---------------------------------------------------------------------------------

FncTestEnding(True, $TestComplete)