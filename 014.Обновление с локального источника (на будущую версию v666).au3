#include <KSVDAutoItTestFramework\KSVDAutoItTestFramework.au3>

; ---- Инициализация необходимых переменных ----

Local $sKSVDSetupFolder = IniRead ( $sCommonConfig, "InstallInformation", "KSVDSetupFolder", "$sKSVDSetupFolder" ) ;путь до папки, содержащей всё необходимое для установки KSVD
Local $sInstallationVersion = IniRead( $sCommonConfig, "InstallInformation", "installationVersion", "$sInstallationVersion") ;версия KSVD с которой выполняется работа
Local $sInstallPath = IniRead ( $sCommonConfig, "InstallInformation", "installPath", "$sInstallPath" ) ;директория установки KSVD
Local $aSetupParameters[10]
$aSetupParameters[0] = "True"
$aSetupParameters[1] = "False"
$aSetupParameters[2] = $sInstallationVersion
$aSetupParameters[3] = $sKSVDSetupFolder
$aSetupParameters[4] = $sInstallPath

;---------------------------------------------------------------------------------
;----- Начало выполнения теста ---------------------------------------------------
;---------------------------------------------------------------------------------
;----- Генерация версии 6.6.6 ----------------------------------------------------
;---------------------------------------------------------------------------------

FncLocalUpdateV666Generator($sKSVDSetupFolder, $sInstallationVersion)

;---------------------------------------------------------------------------------
;----- Проверка установки и текущей версии ---------------------------------------
;---------------------------------------------------------------------------------

If FncCheckKSVDInstallation() Then
    ActionRunArmAdmin()
    ActionLogInArmAdmin("admin")
    If ActionCheckKsvdVersion($sInstallationVersion, False) Then
        _FileWriteLog($sLogFile, "Переустановка KSVD не требуется, т.к. установлена необходимая для теста версия: " & $sInstallationVersion)
    Else
        FncCloseArmAdmin()
        ActionWindowsInstallKSVD($aSetupParameters)
    EndIf
Else
    ActionWindowsInstallKSVD($aSetupParameters)
EndIf

;---------------------------------------------------------------------------------
;----- Добавление Точки Обновления -----------------------------------------------
;---------------------------------------------------------------------------------

ActionAddUpdateSourse("localUpdateToV666", $sKSVDSetupFolder)
sleep(5000)

;---------------------------------------------------------------------------------
;----- Установка Обновления ------------------------------------------------------
;---------------------------------------------------------------------------------

FncCheckAndActivateWindow("Обновление")
$sAction = "Появление окна обновления"
FncScreenCapture($sKeyScreenPath, $sAction)

$sAction = "Клик по кнопке Установить без задержек"
FncMouseClick("left",60,110,1)
sleep(60000)

;---------------------------------------------------------------------------------
;----- Проверка текущей версии ---------------------------------------------------
;---------------------------------------------------------------------------------

ActionRunArmAdmin()
ActionLogInArmAdmin("admin")
ActionCheckKsvdVersion("6.6.6", True)

;---------------------------------------------------------------------------------
;----- Завершение теста ----------------------------------------------------------
;---------------------------------------------------------------------------------

FncTestEnding(True, $TestComplete)